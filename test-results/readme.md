# Purpose
The test results technical module uses the new aggregate test reporting concept of gradle.

# Commands

The set of gradle command line special commands we seldom use:

* To generate aggregated test report           `gradle testCodeCoverageReport`
* To generate aggregated test coverage report  `gradle testCodeCoverageReport`

