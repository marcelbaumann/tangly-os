baseURL = "https://blog.tangly.net/"
title = "tangly Open Source Components"

enableRobotsTXT = true
timeout = 300000

# Hugo allows theme composition (and inheritance). The precedence is from left to right.
theme = ["docsy"]

enableGitInfo = false

contentDir = "content"
defaultContentLanguage = "en"
defaultContentLanguageInSubdir = false
enableMissingTranslationPlaceholders = true

# new security restrictions in hugo
[security]
enableInlineShortcodes = false
[security.exec]
allow = ['^dart-sass-embedded$', '^go$', '^npx$', '^postcss$', '^asciidoctor$', '^lunr$']
osEnv = ['(?i)^(PATH|PATHEXT|APPDATA|TMP|TEMP|TERM|GEM_PATH)$']
[security.funcs]
getenv = ['^HUGO_']
[security.http]
methods = ['(?i)GET|POST']
urls = ['.*']

[taxonomies]
tags = "tags"

# Highlighting config
pygmentsCodeFences = true
pygmentsUseClasses = false
pygmentsUseClassic = false
#pygmentsOptions = "linenos=table"
# See https://help.farbox.com/pygments.html
pygmentsStyle = "tango"

# Configure how URLs look like per section. :slug
[permalinks]
blog = "/:section/:year/:title/"

## Configuration for BlackFriday markdown parser: https://github.com/russross/blackfriday
[blackfriday]
plainIDAnchors = true
hrefTargetBlank = true
angledQuotes = false
latexDashes = true

# Image processing configuration.
[imaging]
resampleFilter = "CatmullRom"
quality = 75
anchor = "smart"

[services]
[services.googleAnalytics]
id = "G-EYDTXKRW9J"

[markup]
[markup.goldmark]
[markup.goldmark.renderer]
unsafe = true
[markup.highlight]
# See a complete list of available styles at https://xyproto.github.io/splash/docs/all.html
style = "tango"
# Uncomment if you want your chosen highlight style used for code blocks without a specified language
guessSyntax = "true"

# rouge is bundled with Asciidoctor 2.0 and should no more be added as extension, the rouge-css style include html marking in the html file (from the
# Asciidoctor expert Dan Allen). The working folder allows asciidoctor-diagram and plantuml to generate diagrams in the correct folder.
# additional extensions not in the official hugo extensions list can be loaded in safeMode is set to unsafe.
# use html5 output because we could not find any useable stylesheet for html5s backend.
[markup.asciidocExt]
safeMode = "unsafe"
workingFolderCurrent = true
backend = "html5"
failureLevel = "fatal"
extensions = ["asciidoctor-diagram", "asciidoctor-bibtex", "asciidoctor-kroki"]
[markup.asciidocExt.attributes]
experimental = true
icons = "font"
imagesdir = "../pics"
source-highlighter = "rouge"
rouge-style = "github"
rouge-css = "style"
copyright = "CC-BY-SA 4.0"
stem = "asciimath"
bibtex-file = "/Users/Shared/Projects/tangly-os/src/main/resources/references.bib"
bibtex-style = "ieee-with-url"
ref-arc42 = "https://arc42.org/[arc42]"
ref-asciidoc = "https://asciidoc.org/[Asciidoc]"
ref-asciidoctor = "https://asciidoctor.org/[Asciidoctor]"
ref-bbrt = "https://bbrt.org/[Beyond Budgeting Round Table]"
ref-bitbucket = "https://bitbucket.org/[Bitbucket]"
ref-c4 = "https://c4model.com/[C4 Model]"
ref-craftsmanship = "https://manifesto.softwarecraftsmanship.org/[Software Craftsmanship]"
ref-devops = "https://en.wikipedia.org/wiki/DevOps[DevOps]"
ref-docsy = "https://www.docsy.dev/[Docsy]"
ref-doxygen = "https://www.doxygen.nl/[Doxygen]"
ref-flow = "https://en.wikipedia.org/wiki/Flow_(psychology)[Flow]"
ref-git = "https://git-scm.com/[Git]"
ref-github = "https://github.com/[GitHub]"
ref-gitlab = "https://gitlab.com/[GitLab]"
ref-gradle = "https://gradle.org/[Gradle Build Tool]"
ref-hermes = "https://www.hermes.admin.ch/en/[HERMES]"
ref-hugo = "https://gohugo.io/[Hugo]"
ref-kaizen = "https://en.wikipedia.org/wiki/Kaizen[Kaizen]"
ref-kanban = "https://en.wikipedia.org/wiki/Kanban[Kanban]"
ref-javadoc = "https://www.oracle.com/java/technologies/javase/javadoc.html[JavaDoc]"
ref-junit5 = "https://junit.org/junit5/[JUnit 5]"
ref-lean = "https://en.wikipedia.org/wiki/Lean_software_development[Lean Software Development]"
ref-less = "https://less.works/[LeSS]"
ref-manifesto = "https://agilemanifesto.org/[Agile Manifesto]"
ref-manifesto-principles = "https://agilemanifesto.org/principles.html[12 Agile Manifesto Principles]"
ref-maven = "https://maven.apache.org/[Maven]"
ref-markdown = "https://www.markdownguide.org/[Markdown]"
ref-mermaid = "https://mermaid.js.org/[Mermaid]"
ref-microstream = "https://microstream.one/[MicroStream]"
ref-openapi = "https://www.openapis.org/[OpenAPI]"
ref-pdca = "https://en.wikipedia.org/wiki/PDCA[Plan-Do-Check-Adapt]"
ref-plantuml = "https://plantuml.com/[plantUML]"
ref-repository = "https://github.com/tangly-team/tangly-os/[Source Code]"
ref-safe = "https://www.scaledagileframework.com/[SAFe]"
ref-scrum = "https://scrumguides.org/[Scrum]"
ref-scrum-alliance = "https://www.scrumalliance.org/[Scrum Alliance]"
ref-scrum-org = "https://www.scrum.org/[Scrum.org]"
ref-scrumguide = "https://www.scrumguides.org/[Scrum Guide]"
ref-site = "https://blog.tangly.net/[Open Source Components]"
ref-sociocracy = "https://sociocracy30.org/[Sociocracy]"
ref-tangly = "https://www.tangly.net/[tangly]"
ref-uml = "https://en.wikipedia.org/wiki/Unified_Modeling_Language[UML]"
ref-uri-blog = "https://blog.tangly.net/[tangly blog]"
ref-uri-prj-doc = "https://blog.tangly.net/[tangly OS documentation]"
ref-uri-prj-issues = "https://bitbucket.org/tangly-team/tangly-os/issues[tangly OS issues]"
ref-uri-prj-repo = "https://github.com/marcelbaumann/tangly-os[tangly OS repository]"
ref-utterances = "https://utteranc.es/[utteranc.es]"
ref-xp = "http://www.extremeprogramming.org/[eXtreme Programming]"

var-current-ver = "0.2.7"
var-current-release-date = "2023-06-30"

languageCode = "en-us"
defaultContentLanguage = "en"
title = "tangly Open Source Components"

[languages]
[languages.en]
title = "tangly Components"
weight = 1
[languages.en.params]
description = "tangly llc Open Source Components"
languageName = "English"


[outputs]
section = ["HTML", "RSS"]

[params.print]
disable_toc = true

# Shortcodes
[sc_attachments]
other = "Attachments"

# Everything below this are Site Params

[params]
copyright = "tangly llc - Attribution 4.0 International (CC BY 4.0)"
# privacy_policy =

time_format_blog = "2006-01-02"
time_format_default = "2006-01-02"
rss_sections = ["blog"]

# flag indicating if the utterances (https://utteranc.es/) should be displayed
comments_utterances = true

# GitHub repository name where all comments are stored. The repository can be the same as the website repository or a different one.
comments_utterances_repo = "tangly-team/tangly-team-comments"

# Encoding used to map a site page to a GitHub issue. See utterances (https://utteranc.es/) documentation.
comments_utterances_issue_term = "pathname"

# Theme used by utterance on the website. See utterances (https://utteranc.es/) documentation.
comments_utterances_theme = "github-light"

# images = ["images/product-illustration.png"]

# Flag used in the "version-banner" partial to decide whether to display a
# banner on every page indicating that this is an archived version of the docs.
# Set this flag to "true" if you want to display the banner.
archived_version = false

# The version number for the version of the docs represented in this doc set.
# Used in the "version-banner" partial to display a version number for the
# current doc set.
version = "2022.06"

# A link to latest version of the docs. Used in the "version-banner" partial to
# point people to the main doc site.
url_latest_version = "https://blog.tangly.net"

# gcs_engine_id =
algolia_docsearch = false

offlineSearch = true
offlineSearchSummaryLength = 200
offlineSearchMaxResults = 50

# Trick to have a submenu in the main menu of the site
version_menu = "Repositories"

[[params.versions]]
version = "GitHub"
url = "https://github.com/tangly-team/tangly-os/"

[[params.versions]]
version = "GitLab"
url = "https://gitlab.com/marcelbaumann/tangly-os"

[[params.versions]]
version = "Bitbucket"
url = "https://bitbucket.org/tangly-team/tangly-os/"

# User interface configuration
[params.ui]
# Enable to show the side bar menu in its compact state.
sidebar_menu_compact = false
# Enable to show the side bar menu as foldable.
sidebar_menu_foldable = true
#  Set to true to disable breadcrumb navigation.
breadcrumb_disable = false
#  Set to true to hide the sidebar search box (the top nav search box will still be displayed if search is enabled)
sidebar_search_disable = false
#  Set to false if you don't want to display a logo (/assets/icons/logo.svg) in the top nav bar
navbar_logo = true
# Set to true to disable the About link in the site footer
footer_about_disable = false

[params.ui.feedback]
enable = false
yes = 'Glad to hear it! Please <a href="https://bitbucket.org/tangly-team/tangly-os/issues/new">tell us how we can improve</a>.'
no = 'Sorry to hear that. Please <a href="https://bitbucket.org/tangly-team/tangly-os/issues/new">tell us how we can improve</a>.'

[params.ui.readingtime]
enable = false

[params.links]
[[params.links.user]]
name = "Issue Tracker"
url = "https://github.com/tangly-team/tangly-os/issues"
icon = "github-square"
desc = "Discussion and Improvements Suggestions"
[[params.links.user]]
name = "Documentation"
url = "https://blog.tangly.net/"
icon = "github-square"
desc = "Documentation of the open source components (this site)"
[[params.links.user]]
name = "Groups"
url = "https://groups.google.com/g/tangly-os-components"
icon = "users-cog"
desc = "Discussion groups of the open source components"
[[params.links.developer]]
name = "Github"
url = "https://github.com/tangly-team/tangly-os/"
icon = "github"
desc = "Development takes place in this repository"
