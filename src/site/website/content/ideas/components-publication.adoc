---
title: "Components Publication"
date: 2019-05-01
weight: 35
description: Workflow to publish open source components
---

== Components Publication
:author: Marcel Baumann
:email: <marcel.baumann@tangly.net>
:homepage: https://www.tangly.net/
:company: https://www.tangly.net/[tangly llc]
:toc:

=== Regular Git Repository Update

. Push the changes to the {ref-github} repository {ref-repository}.
.. Verify that the build job in the CI pipeline was successful.
. Push the changes to the GitHub repository.
_The statistics for developer activities will be automatically updated._

=== Maven Central Publication

The following process is used to publish {ref-tangly} open source component on https://mvnrepository.com/repos/central[Maven Central].

. Publish artifacts of a component with the script `uploadToMavenCentral.sh`.
Do not use the gradle task `gradle publishAllPublicationsToMavenRepository -Pmode=prod`.
Sonatype cannot handle a parallel upload of multiple components and simply close the connections during upload (Status 2020-Q2).
.. Pre-activity: test publishing with `gradle publishToMavenLocal`
.. Pre-activity: check on bitbucket the product compiles through the *CI* continuous integration pipeline
.. Post-activity: *tag repository* on git to identify the version of the published packages.
The tag version semantic part *must* be the same as the package version on https://mvnrepository.com/repos/central[Maven Central]
The tag is of the form -{AcronymComponent}-MajorVersion-MinorVersion-PatchVersion-
.. Verify with `git tag label` and `git push origin --tags`
. Log into https://oss.sonatype.org/#welcome[Sonatype Maven Central Repository]
.. Go to https://oss.sonatype.org/#stagingRepositories[Staging Repositories]
.. Find your temporary repository at the end of the list
.. Review the content of the repository
.. Close the repository-button on tool list-
.. Close stage is initiated and checks are performed.
If everything was fine, the content was published.
.. Release the artifact-button on the tool list-
. See <<website-publication>> to update the website

[CAUTION]
====
It takes up to one day until the component is visible in Maven Central website upon publication.

On the positive side, the last publications were performed in less than 30 minutes.
====

Below the dependencies between tangly open source components as defined in the multi-modules {ref-gradle} build file.

[plantuml,tangly-componentsDependencies,svg,svg-type="interactive"]
....
package net.tangly.bdd [[https://blog.tangly.net/docs/bdd/]] {
}

package net.tangly.commons [[https://blog.tangly.net/docs/commons/]] {
}

package net.tangly.core [[https://blog.tangly.net/docs/core/]] {
}

package net.tangly.dev [[https://blog.tangly.net/docs/dev/]] {
}
net.tangly.dev --> net.tangly.commons

package net.tangly.fsm [[https://blog.tangly.net/docs/fsm/]] {
}

package net.tangly.gleam [[https://blog.tangly.net/docs/gleam/]] {
}
net.tangly.gleam --> net.tangly.commons

package net.tangly.ports [[https://blog.tangly.net/docs/ports/]] {
}

package net.tangly.ui [[https://blog.tangly.net/docs/ui/]] {
}

package net.tangly.erp.crm [[https://blog.tangly.netdocs/domains/crm]] {
}

package net.tangly.erp.invoices [[https://blog.tangly.netdocs/domains/invoices]] {
}

package net.tangly.erp.ledger [[https://blog.tangly.netdocs/domains/ledger]] {
}

package net.tangly.erp.products [[https://blog.tangly.netdocs/domains/products]] {
}

package net.tangly.erp.shared [[https://blog.tangly.netdocs/domains/shared]] {
}

package net.tangly.erp.ui [[https://blog.tangly.netdocs/domains/ui]] {
}

....

=== Decisions

. The open source components are stored in a mono repository.
Each component is declared as a gradle module.
. We use de facto standard Git.
. We are hosted at {ref-github}
footnote:[We moved from {ref-bitbucket} to {ref-github} in 2022.
The reasons were that the new platform supports publishing of static website with a lot more functionalities.
The new agile project management functionality with Scrum boards, sprint iterations and insights report is very interesting.].

The synchronization is performed with the command

[source,shell]
----
git push --prune https://github.com/marcelbaumann/tangly-os.git +refs/remotes/origin/*:refs/heads/* +refs/tags/*:refs/tags/* <1>

git remote <2>

git push origin-bitbucket <3>
----

<1> Push the repository with all remotes, references and tags to GitHub specified repository
<2> List all remotes associated with this git repository
<3> Push the master branch to the _origin-bitbucket_ remote repository.
The push shall trigger the continuous integration pipeline

You must create a token for developer account to be able to push changes and trigger the CI pipeline.
The username is your account username The password is the generated token
footnote:[This is necessary if you have enabled two factors authentication.
The other approach is to generate an SSH key and perform all operations other SSH.].
Store it in a safe place.

==== Findings

. We needed to manually publish the public key on https://keys.openpgp.org/[keys.openpgp.org]
. Gradle build file has a condition including disabling the signing plugin on the CI pipeline because keys and associated authentication are not available.
I decided not to upload private keys to https://bitbucket.org/[bitbucket].
See the documentation at the beginning of the Gradle build file for details.

IMPORTANT: Set the property if you want to sign the component artifacts and publish to Maven Central.

[#website-publication]
=== Static Website Publication

==== Introduction

The static website uses {ref-asciidoc} to write all documents.

The static website uses  {ref-hugo} as site generator.
The Hugo theme is {ref-docsy}.

==== Configure Asciidoctor

The new version of the {ref-hugo} support configuration of {ref-asciidoctor} through config.toml therefore no manipulation of asciidoctor is needed.
As a bonus, the files generated by PlantUml are created in the correct folder.
We still need to install the referenced packages.

[source,shell]
----
sudo gem install asciidoctor-diagram <1>
sudo gem install asciidoctor-bibtex <2>
sudo gem install asciimath <3>
----

<1> Provides support for all diagrams such as plantUML and mermaid.
<2> Provides support for formal bibliography references.
<3> Provides support for mathematical and logical expressions in documents.

=== Docsy Tailoring

The docsy theme is missing some features.
It does not support asciidoc styling or commenting blogs.
Our extensions follow the official rules how a theme can be tailored and expended.

The major changes are:

* Inclusion of the styling sheet _asciidoctor.scss_ to style asciidoc documents
footnote:[I asked for a change request on GitHub for Docsy to better support asciidoc documents.
The Docsy team decided that their priorities are different and rejected the request.].
* Inclusion of the shortcodes extension _shortcodes.html_ layouts and the style file _shortcodes.scss_ to support file attachments in the website
footnote:[This change is defined as a merge request.
The pull request is quite old and seems to have a low priority.].
* Extension of the blog section with _comments-uttereances.html_ to support comments on blog articles
footnote:[The current setup of the template does not allow selection of a comment solution per configuration.].
* Changes in the header and footer partials to display mathematical expressions and well-styled copyright
footnote:[An issue report exists on GitHub about the copyright issue.].

All changes are defined in the assets and layouts folders as part of our website content.
Hugo's algorithms select local overwritten files and ignore the corresponding theme files.\

[NOTE]
====
The current structure of our extensions follows the official approach how a Hugo module should be extended.
We do not change any files in the theme.
All changes are defined locally.
We use the selection algorithm of Hugo to activate our tailoring and extensions.
====

==== Create Website

The tangly open source components website is hosted under https://blog.tangly.net/docs[Documentation].

Read the instructions in the asciidoctor script file under the scripts' folder.
Four scripts are provided to generate the Hugo static site with associated structure and theme.

. the script _initiateHugo.sh_ creates the layout of the site and retrieves the theme.
. the script _populateHugo.sh_ populates the site with our content.

Upon completion of local development, you must start a local Hugo server and generate the indexing files for https://lunrjs.com/[lunr] search.
Before uploading the site, stop the Hugo local server.

. the script _completeHugo.sh_ generates the static pictures and copies them to the static folder and publish the whole site on bitbucket.

Upon completion, the site is published on the web for all.

==== Set JDK Version under macOS

Install regular Java JDK such as the Oracle or OpenJDK distribution.
They will be located under _/Library/Java/JavaVirtualMachines_.

If using bash shell, add the following commands to your .bash_profile.

[source,shell]
----
alias java17 = "export JAVA_HOME=`/usr/libexec/java_home -v 17`; java -version"
alias java19 = "export JAVA_HOME=`/usr/libexec/java_home -v 19`; java -version"
alias java20 = "export JAVA_HOME=`/usr/libexec/java_home -v 20`; java -version"
alias java21 = "export JAVA_HOME=`/usr/libexec/java_home -v 21`; java -version"
----

You can set the exact version such as _17.0.1_ to precisely select a JDK or set an overall version such as _17_ to select a generic version.

After restarting your terminal, the command _java17_ will set JDK 17 to default JDK (if installed on your macOS).

The list of JDK can be found with

[source,shell]
----
/usr/libexec/java_home -V
----

==== Create Docker Distribution

Generate the ERP application as a production release.

[source,shell]
----
gradle install -DproductionMode=true <1>
gradle install -Pproduction
unzip ./build/distributions/net.tangly.erp.ui-<version>.zip <2>
docker build -t erp-docker . <3>
docker run -ti -p 8080:8080 erp-docker <4>
----

<1> Generates a production distribution stored under ./build/distributions
<2> Unzip the packed distribution because the docker base image does not contain unzip utility
<3> Build the docker image named _erp-docker_
<4> Run the docker image _erp-docker_ and map the image port to 8080



