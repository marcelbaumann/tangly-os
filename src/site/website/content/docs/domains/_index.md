---
title: "ERP Bounded Domains"
linkTitle: "ERP Domains"
weight: 80
---

The bounded domain models to build an enterprise resources planning application for services companies.

The architecture of the ERP and associated bounded domains is documented [here](/docs/domains/_design/architecture)
