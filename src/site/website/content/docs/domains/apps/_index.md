---
title: "Applications - Apps Bounded Domain"
linkTitle: "Apps"
weight: 15
---

The readme, user manual, documentation of the component and architecture consideration of the components.

The API documentation can be found under [Javadoc](/docs/bdd/api-bdd/index.html).


