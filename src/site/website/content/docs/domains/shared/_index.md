---
title: "Shared - Shared Kernel"
linkTitle: "Shared"
weight: 45
---

The readme, user manual, documentation of the component and architecture consideration of the components.

The API documentation can be found under [Javadoc](/docs/domains/shared/api-shared/index.html).
