---
title: "ERP-UI - UI for ERP Bounded Domains"
linkTitle: "ERP-UI"
weight: 50
---

The readme, user manual, documentation of the component and architecture consideration of the components.

The API documentation can be found under [Javadoc](/docs/domains/ui/api-ui/index.html).
