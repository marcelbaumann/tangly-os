---
title: "Core - Core model abstractions"
linkTitle: "Core"
weight: 20
---

The readme, user manual, documentation of the component and architecture consideration of the components.

The API documentation can be found under [Javadoc](/docs/core/api-core/index.html).
