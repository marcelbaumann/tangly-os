/*
 * Copyright 2021-2022 Marcel Baumann
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 *          https://apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

/**
 * Provides regular abstractions for CRM domain and associated abstractions. The key entities are natural entities to represent human beings and legal
 * entities to represent various types of organizations with a recognized legal standing.
 * <p>A natural entity represents a human being with a photo displaying the physical appearance.</p>
 */
package net.tangly.core.crm;
