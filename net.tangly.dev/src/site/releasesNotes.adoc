---
title: "Release Notes"
date: 2019-05-01
weight: 20
---
:source-highlighter: pygments

== DEV Library Change Logs

This document provides a high-level view of the changes introduced in the DEV library by release.
For a detailed view of what has changed, refer to the https://bitbucket.org/tangly-team/tangly-os[repository] commits.

=== 0.2.6 (2021-08-31)

* Update of the dependencies

=== 0.2.5 (2020-08-31)

* Initial release of the library
